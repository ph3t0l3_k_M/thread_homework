package com.psybergate.grad2020.ce1a.main;

/**
 * @author motheo.sekgetho
 * @since 20 Apr 2020
 */
public class DemoExercise1 {

  public static void main(String[] args) {
    Thread t1 = new MyThread("Thread1", 4);
    Thread t2 = new MyThread("Thread2", 10);
    t1.start();
    t2.start();
  }

  int helper(int i) {
    return ++i;
  }
}

class MyThread extends Thread {

  private int value;

  public MyThread(String name, int value) {
    super(name);
    this.value = value;
  }

  @Override
  public void run() {
    System.out.println(String.format("%1s value : %2d", getName(), worker(this.value)));
    System.out.println();
    foo();
  }

  private int worker(int value) {
    return (int) Math.pow(value, 2);
  }

  private void foo() {
    System.out.println("foo " + getName());
    who();
  }

  private void who() {
    System.out.println("who " + getName());
    go();
  }

  private void go() {
    System.out.println("go " + getName());
    doYou();
  }

  private void doYou() {
    System.out.println("doYou " + getName());
    booboo();
  }

  private void booboo() {
    System.out.println("booboo " + getName());
    throw new RuntimeException();
  }

}
